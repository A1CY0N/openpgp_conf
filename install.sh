apt install gnupg-curl
apt install parcimonie
apt install kleopatra
apt install hopenpgp-tools

mv gpg.conf ~/.gnupg/gpg.conf
mv dirmngr.conf ~/.gnupg/dirmngr.conf
wget https://sks-keyservers.net/sks-keyservers.netCA.pem -P ~/.gnupg
