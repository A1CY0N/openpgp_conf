#!/bin/bash
# ---------------------------------------------------------------------------
# Preamble - This script allows you to temporarily and securely use your    |
# secret PGP master key stored on a usb device.                             | 
# ---------------------------------------------------------------------------
# Purpose - Script to use secret PGP keys
# Execution - This script must be run by a less privileged user.
# Author - A1CY0N
# ------------------------------------------------------------------

# Label of your USB device 
LABEL=mystick

# Private key path on the USB device
KEYFILE=alice-private-keys.asc

# Identify the device file corresponding to your USB stick
device=$(/sbin/blkid -L $LABEL)

if [ -n "$device" ]; then
    # Mount the device
    udisksctl mount --block-device $device

    # Create temporary GnuPG home directory
    tmpdir=$(mktemp -d -p $XDG_RUNTIME_DIR gpg.XXXXXX)

    # Import the private keys
    gpg2 --homedir $tmpdir --import /run/media/$USER/$LABEL/$KEYFILE

    # Unmount the stick
    udisksctl unmount --block-device $device

    # Launch GnuPG from the temporary directory,
    # with the default public keyring
    # and with any arguments given to us on the command line
    gpg2 --homedir $tmpdir --keyring ${GNUPGHOME:-$HOME/.gnupg}/pubring.kbx $@

    # Cleaning directory
    [ -f $tmpdir/S.gpg-agent ] && gpg-connect-agent --homedir $tmpdir KILLAGENT /bye
    find $tmpdir -type f | xargs shred -v -n 1
    sync
    find $tmpdir -type f | xargs shred -v -n 0 -z -u
fi